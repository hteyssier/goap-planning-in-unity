using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractAction {

    private Dictionary<string,bool> pre_conditions;
    private Dictionary<string,bool> effects;
    
    private bool need_to_move_to_perform_action = false;
    private bool arrived_at_location = false;
    private bool finished_action = false;
    //public Gamebool target;
    public float cost = 1f;

    public AbstractAction(){
        pre_conditions = new Dictionary<string, bool> ();
		effects = new Dictionary<string, bool> ();
    }

    // public abstract bool Perfom();

    public bool CheckPreConditions(Dictionary<string,bool> world_state){

        // for each precondition, we check if the condition in world state is different
        foreach(KeyValuePair<string,bool> kvp_pc in pre_conditions){
            if(world_state[kvp_pc.Key] != kvp_pc.Value){
                return false;
            }
        }
        return true;
    }

    // public void ResetFlags(){
    //     finished_action = false;
    //     arrived_at_location = false;
    // }

    public void SetFinishedAction(bool b){
        finished_action = b;
    }

    public void SetArrivedAtLocation(){
        arrived_at_location = true;
    }

    public void SetNeedToMove(bool b){
        need_to_move_to_perform_action = b;
    }

    public void SetCost(float i){
        cost = i;
    }

    public void AddPreConditons(string key, bool value){
        pre_conditions.Add(key, value);
    }

    // public void RemovePreCondition(string key){
    //     foreach(KeyValuePair<string, bool> entry in pre_conditions){
    //         if(entry.Key.Equals(key)){
    //             pre_conditions.Remove(entry);
    //         }
    //     }
    // }

    public void AddEffect(string key, bool value){
        effects.Add(key, value);
    }

    // public void RemovePostCondition(string key){
    //     foreach(KeyValuePair<string, bool> entry in effects){
    //         if(entry.Key.Equals(key)){
    //             effects.Remove(entry);
    //         }
    //     }
    // }

    public bool FinishedAction{
        get {
            return finished_action;
        }
    }

    public bool ArrivedAtLocation{
        get {
            return arrived_at_location;
        }
    }

    public bool NeedToMove{
        get {
            return need_to_move_to_perform_action;
        }
    }
    public Dictionary<string, bool> Pre_Conditions {
		get {
			return pre_conditions;
		}
    }

    public Dictionary<string, bool> Effects {
		get {
			return effects;
		}
    }

    public float Cost {
        get {
            return cost;
        }
    }




}
