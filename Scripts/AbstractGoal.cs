using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractGoal
{
    private Dictionary<string,bool> pre_conditions;
    private Dictionary<string,bool> effects;

    public AbstractGoal(){
        pre_conditions = new Dictionary<string, bool> ();
		effects = new Dictionary<string, bool> ();
    }

    public bool CheckGoalPreConditions(Dictionary<string,bool> world_state){

        // for each precondition, we check if the condition in world state is different
        foreach(KeyValuePair<string,bool> kvp_pc in pre_conditions){
            if(world_state[kvp_pc.Key] != kvp_pc.Value){
                return false;
            }
        }
        return true;
    }

    public void AddPreConditons(string key, bool value){
        pre_conditions.Add(key, value);
    }

    public void AddEffect(string key, bool value){
        effects.Add(key, value);
    }

    public Dictionary<string, bool> Pre_Conditions {
		get {
			return pre_conditions;
		}
    }

    public Dictionary<string, bool> Effects {
		get {
			return effects;
		}
    }

}
