using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activator_Behaviour : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public IEnumerator SetInactiveForSeconds(GameObject obj, float time){
        obj.SetActive(false);
        yield return new WaitForSeconds(time);
        obj.SetActive(true); 

    }
}
