using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideInTreeAction : AbstractAction
{
    public HideInTreeAction(){
        AddPreConditons("isPlayerNearby", true);
        AddPreConditons("isInTree", false);

        AddEffect("hasEnergy", false);
        AddEffect("isPlayerNearby", false);
        AddEffect("isInTree", true);

        SetNeedToMove(true);
    }
}
