using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveTreeAction : AbstractAction
{
    public LeaveTreeAction(){
        AddPreConditons("isInTree", true);

        AddEffect("isInTree", false);
        
        SetNeedToMove(false);
    }
}
