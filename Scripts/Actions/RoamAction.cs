using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoamAction : AbstractAction
{
    public RoamAction (){
        AddPreConditons("hasEnergy", true);
        AddPreConditons("isPlayerNearby", false);
        AddPreConditons("isInTree",false);

        AddEffect("hasEnergy", false);
        AddEffect("isBackAtHomeTree", false);
        SetNeedToMove(true);

        SetCost(2f);
    }
}
