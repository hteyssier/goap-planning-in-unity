using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepositeFoodAction : AbstractAction
{
    public DepositeFoodAction(){
        AddPreConditons("hasEnergy", true);
        AddPreConditons("hasRoomForFood", false);
        AddPreConditons("isPlayerNearby", false);
        AddPreConditons("isInTree", false);

        AddEffect("hasEnergy", false);
        AddEffect("hasRoomForFood", true);
        AddEffect("isBackAtHomeTree", true);

        SetNeedToMove(true);

        SetCost(2f);
    }
}
