using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestAction : AbstractAction
{
    public RestAction(){
        AddPreConditons("hasEnergy", false);
        AddPreConditons("isPlayerNearby", false);
        
        AddEffect("hasEnergy", true);

        SetNeedToMove(false);

        SetCost(0f);
    }
}
