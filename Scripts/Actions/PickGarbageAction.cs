using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickGarbageAction : AbstractAction
{

    public PickGarbageAction(){
        AddPreConditons("hasEnergy", true);
        AddPreConditons("hasGarbageInMemory", true);
        AddPreConditons("hasRoomForFood", true);
        AddPreConditons("hasNutInPocket", false);
        AddPreConditons("isPlayerNearby", false);
        AddPreConditons("isInTree", false);

        AddEffect("hasRoomForFood", false);

        // will need to decrease the number of garbage in memory by 1.
        // will need to increment the number of garbage content in poket by 1.

        SetNeedToMove(true);

        SetCost(2f);
    }


}
