using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickNutsAction : AbstractAction
{
    
    //private bool collected = false;

    public PickNutsAction(){
        AddPreConditons("hasEnergy", true);
        AddPreConditons("hasNutsInMemory", true);
        AddPreConditons("hasRoomForFood", true);
        AddPreConditons("isPlayerNearby", false);
        AddPreConditons("isInTree", false);

        AddEffect("hasRoomForFood", false);
        AddEffect("hasNutInPocket", true);

        // will need to decrease the number of nuts in memory by 3.
        // will need to increment the number of nuts in poket by 3.

        SetNeedToMove(true);
        SetCost(3f);
    }
}
