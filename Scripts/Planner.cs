using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Planner
{
    private List<AbstractGoal> goal_list;
    private List<AbstractAction> action_list;

    public Planner(){
        goal_list = new List<AbstractGoal>();
        action_list = new List<AbstractAction>();
    }


    public AbstractGoal ChooseAGoal(Dictionary<string,bool> current_world_state){
        List<AbstractGoal> available_goals = new List<AbstractGoal>();


        // For each goal, we check if the world state permits to do it
        foreach(AbstractGoal a_goal in goal_list){ 
            if(a_goal.CheckGoalPreConditions(current_world_state)){
                available_goals.Add(a_goal);
            }
        }

        //Debug.Log("Choosing goal, nb of goals available: " + available_goals.Count);
        try{
            return available_goals[0];
        } catch (Exception e){
            Debug.Log("Error while choosing the goal: " +e);
        }
        return null;
    }

    public Stack<AbstractAction> GetAPlan(Dictionary<string,bool> world_state, AbstractGoal goal){

        // We make a copy of the world_state
        Dictionary<string,bool> current_world_state = new Dictionary<string,bool>();
        foreach (KeyValuePair<string,bool> state in world_state){
            current_world_state.Add(state.Key, state.Value);
        } 
        
        // We create a root node with the current world state  
        Node root = new Node(current_world_state, goal);

        // We build the graph of actions 
        BuildGraphLayers(root, goal);

        // We search the graph for the best goal node 
        Node best_node = SearchGraph(root);

        Stack<AbstractAction> plan = new Stack<AbstractAction>();
        Node node_ptr = best_node;

        // if we found a goal node, we build the stack
        if(best_node != null){
            while(node_ptr != root){
                plan.Push(node_ptr.Action);
                node_ptr = node_ptr.Parent;
            }
        }

        return plan;
    }

    private Node SearchGraph(Node parent){

        List<Node> goal_leaves = new List<Node>();
        Queue<Node> nodes_to_visit = new Queue<Node>();
        nodes_to_visit.Enqueue(parent);

        // We gather all nodes that reached the goal state 
        while(nodes_to_visit.Count != 0){
            Node a_node = nodes_to_visit.Dequeue();
            foreach(Node child in a_node.Children){
                nodes_to_visit.Enqueue(child);
                if(child.has_reached_goal_state){
                    goal_leaves.Add(child);
                }
            }
        }

        // We select the best goal
        Node to_return = null;
        float best_cost = 10000;
        foreach( Node n in goal_leaves){
            if(n.Cost < best_cost){
                to_return = n;
                best_cost = n.Cost;
            }
        }

        return to_return;
    }

    private void BuildGraphLayers(Node parent, AbstractGoal goal){
        
        // We get all legal action
        List<AbstractAction> legal_actions = GetLegalAction(parent.Node_State);
        
        // for each action we create a node (a child)
        List<Node> children = new List<Node>();
        foreach(AbstractAction action in legal_actions){
            children.Add(new Node(parent, action, parent.Node_State, goal));
        }
        parent.SetChildren(children);

        // for each node, we check if it fullfils the goal
        // if fulfills the goal, we don't look over the nodes children
        // otherwise, we recurse and try to get more children
        foreach(Node child in children){
            child.CheckIfGoalState();
            if(child.has_reached_goal_state == false && child.Depth < 10){
                BuildGraphLayers(child, goal);
            }
        }
    }

    private List<AbstractAction> GetLegalAction(Dictionary<string,bool> world_state){

        // from all actions available, we check of their precondtions match the world state
        List<AbstractAction> legal_actions = new List<AbstractAction>();
        foreach(AbstractAction action in action_list){
            if(action.CheckPreConditions(world_state)){

                if(action.GetType() == typeof(PickNutsAction)){
                    legal_actions.Add(new PickNutsAction());
                }
                else if (action.GetType() == typeof(PickGarbageAction)){
                    legal_actions.Add(new PickGarbageAction());
                }
                else if (action.GetType() == typeof(DepositeFoodAction)){
                    legal_actions.Add(new DepositeFoodAction());
                }
                else if (action.GetType() == typeof(HideInTreeAction)){
                    legal_actions.Add(new HideInTreeAction());
                }
                else if (action.GetType() == typeof(RoamAction)){
                    legal_actions.Add(new RoamAction());
                }
                else if (action.GetType() == typeof(RestAction)){
                    legal_actions.Add(new RestAction());
                }
                else if (action.GetType() == typeof(LeaveTreeAction)){
                    legal_actions.Add(new LeaveTreeAction());
                }
            }
        }
        return legal_actions;
    }

    public void AddAction(AbstractAction an_action){
        action_list.Add(an_action);
    }

    public void AddGoal(AbstractGoal a_goal){
        goal_list.Add(a_goal);
    }

    public List<AbstractGoal> GoalList{
        get {
            return goal_list;
        }
    }
}
