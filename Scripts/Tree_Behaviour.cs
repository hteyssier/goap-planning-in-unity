using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Tree_Behaviour : MonoBehaviour
{
    public GameObject nut_prefab;
    public GameObject squirrel_to_show_prefab;

    private List<GameObject> tree_nuts;
    private bool wait;
    private bool has_a_squirrel;
    private GameObject hidden_squirrel; 

    // Start is called before the first frame update
    void Start()
    {
        tree_nuts = new List<GameObject>();
        wait=false;
        has_a_squirrel=false;

        Vector3 tree_pos = gameObject.transform.position;
        hidden_squirrel = Instantiate(squirrel_to_show_prefab, new Vector3(tree_pos.x, tree_pos.y + 11.2f, tree_pos.z), Quaternion.identity);
        hidden_squirrel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(!wait){
            if(tree_nuts.Count < 5){
                
                int r = 6;
                float theta = UnityEngine.Random.Range(0.0f, 1.0f) * 2f * (float)Math.PI;
                float x_coord = gameObject.transform.position.x + r * (float)Math.Cos(theta);
                float z_coord = gameObject.transform.position.z + r * (float)Math.Sin(theta);

                GameObject nut = Instantiate(nut_prefab, new Vector3(x_coord, 1.0f, z_coord), Quaternion.identity);
                nut.transform.SetParent(gameObject.transform);
                tree_nuts.Add(nut);
            }
            wait=true;
            StartCoroutine(WaitFor2Seconds());
        }
        
    }

    private IEnumerator WaitFor2Seconds(){
        yield return new WaitForSeconds(2);
        wait=false;
    }

    public void ShowHiddenSquirrel(bool b){
        if (b){
            hidden_squirrel.SetActive(true);
        } else {
            hidden_squirrel.SetActive(false);
        }

    }

    public void RemoveNut(GameObject nut){
        tree_nuts.Remove(nut);
    }

    public void SetHasASquirrel(bool b){
        has_a_squirrel=b;
    }

    public bool HasSquirrel{
        get {
            return has_a_squirrel;
        }
    }


}
