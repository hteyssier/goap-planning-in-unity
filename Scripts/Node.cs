using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    private Node parent;
    private List<Node> children;
    private float cost;
    private Dictionary<string,bool> node_state;
    private AbstractAction action;
    private Dictionary<string,bool> goal_state;
    private int depth;
    public bool has_reached_goal_state;

    public Node(Dictionary<string,bool> world_state, AbstractGoal goal){
        parent = null;
        children = new List<Node>();
        cost = 0f;
        node_state = world_state;
        action = null;
        goal_state = goal.Effects;
        has_reached_goal_state = false;
        depth = 0;
        
    }

    public Node(Node parent, AbstractAction action, Dictionary<string,bool> world_state, AbstractGoal goal){
        this.parent = parent;
        this.action = action;
        this.cost = parent.Cost + action.Cost;
        children = new List<Node>();
        goal_state = goal.Effects;
        has_reached_goal_state = false;
        depth = parent.Depth + 1;

        node_state = ApplyActionEffect(world_state);
    } 

    public bool CheckIfGoalState(){
        foreach(KeyValuePair<string,bool> kvp in goal_state){
            if(kvp.Value != node_state[kvp.Key]){
                return false;
            }
        }
        has_reached_goal_state = true;
        return true;
    }

    public void SetChildren(List<Node> children){
        this.children = children;
    }

    // We apply the action effect to the world state.
    private Dictionary<string,bool> ApplyActionEffect(Dictionary<string,bool> world_state){

        Dictionary<string,bool> world_state_after_effect = new Dictionary<string,bool>();

        // we make a copy of the current world state
        foreach (KeyValuePair<string,bool> state in world_state){
            world_state_after_effect.Add(state.Key, state.Value);
        }

        // we apply each effects on the world state
        foreach (KeyValuePair<string, bool> effect in action.Effects){
            world_state_after_effect[effect.Key] = effect.Value;
        }

        return world_state_after_effect;
    }

    public int Depth {
        get {
            return depth;
        }
    }

    public AbstractAction Action {
        get {
            return action;
        }
    }

    public Node Parent{
        get{
            return parent;
        }
    }

    public Dictionary<string,bool> Node_State {
        get {
            return node_state;
        }
    }

    public List<Node> Children {
        get {
            return children;
        }
    }

    public float Cost {
        get {
            return cost;
        }
    }

    
}
