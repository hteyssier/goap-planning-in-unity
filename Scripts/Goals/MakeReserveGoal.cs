using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeReserveGoal : AbstractGoal
{
    public MakeReserveGoal(){
        AddPreConditons("hasEnergy", true);
        AddPreConditons("hasSomeFoodInMemory", true);
        AddPreConditons("hasRoomForFood", true);
        AddPreConditons("isPlayerNearby", false);

        AddEffect("hasEnergy", false);
        AddEffect("hasRoomForFood", true);
        AddEffect("isBackAtHomeTree", true);
    }
}
