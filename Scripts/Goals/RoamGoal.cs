using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoamGoal : AbstractGoal
{
    public RoamGoal(){
        AddPreConditons("hasEnergy", true);
        AddPreConditons("isPlayerNearby", false);

        AddEffect("hasEnergy", false);
        AddEffect("isBackAtHomeTree", false);
    }
}
