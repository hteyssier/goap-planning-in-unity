using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestGoal : AbstractGoal
{
    public RestGoal(){
        AddPreConditons("hasEnergy", false);
        AddPreConditons("isPlayerNearby", false);
        
        AddEffect("hasEnergy", true);
    }
}
