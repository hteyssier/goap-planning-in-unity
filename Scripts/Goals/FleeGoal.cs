using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeGoal : AbstractGoal
{
    public FleeGoal(){
        AddPreConditons("isPlayerNearby", true);
        AddPreConditons("isInTree", false);

        AddEffect("hasEnergy", false);
        AddEffect("isPlayerNearby", false);
        AddEffect("isInTree", true);
    }
}
