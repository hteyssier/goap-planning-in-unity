using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Squirrel_Behaviour : MonoBehaviour
{
    private Vector3 home_tree_pos;
    public GameObject garbage_content_prefab;

    private Vector3 goal_position;
    private NavMeshAgent agent;

    // Memory
    private Queue<GameObject> nuts_in_memory;
    private Queue<GameObject> garbage_in_memory;
    private List<GameObject> food_in_pocket;
    private List<GameObject> food_at_home_tree;
    private GameObject closest_tree_in_memory;

    // Planning
    private Stack<AbstractAction> plan;
    private AbstractGoal current_goal;
    private Dictionary<string,bool> world_state;
    private bool hasEnergy;
    private bool isPlayerNearby;
    private bool isInTree;
    private bool isBackAtHomeTree;
    private bool hasSomeFoodInMemory;
    private Planner planner;

    private string plan_status;

    // Perfoming plan
    private bool has_a_plan;
    //private bool performing_action;
    private bool moving;
    private AbstractAction current_action;

    // Helper variable
    private bool wait;


    // Start is called before the first frame update
    void Start()
    {
        home_tree_pos = gameObject.transform.position;
        agent = GetComponent<NavMeshAgent>();
        nuts_in_memory = new Queue<GameObject>();
        garbage_in_memory = new Queue<GameObject>();
        food_in_pocket = new List<GameObject>();
        food_at_home_tree = new List<GameObject>();
        closest_tree_in_memory = null;
        plan = new Stack<AbstractAction>();
        world_state = new Dictionary<string,bool>();
        plan_status = "";

        has_a_plan = false;
        //performing_action = false;
        moving = false;
        current_action = null;

        wait = false;

        hasEnergy = true;
        isPlayerNearby = false;
        isInTree = false;
        isBackAtHomeTree = false;
        hasSomeFoodInMemory=false;

        // Initializing the world state
        world_state.Add("hasEnergy", false);
        world_state.Add("hasSomeFoodInMemory",false);
        world_state.Add("hasNutsInMemory", false);
        world_state.Add("hasGarbageInMemory", false);
        world_state.Add("hasRoomForFood", false);
        world_state.Add("hasNutInPocket", false);
        world_state.Add("isPlayerNearby", false);
        world_state.Add("isInTree", false);
        world_state.Add("isBackAtHomeTree",false);

        // Initializing the planner
        planner = new Planner();
        planner.AddGoal(new FleeGoal());
        planner.AddGoal(new MakeReserveGoal());
        planner.AddGoal(new RoamGoal());
        planner.AddGoal(new RestGoal());
        planner.AddAction(new PickNutsAction());
        planner.AddAction(new PickGarbageAction());
        planner.AddAction(new DepositeFoodAction());
        planner.AddAction(new HideInTreeAction());
        planner.AddAction(new LeaveTreeAction());
        planner.AddAction(new RestAction());
        planner.AddAction(new RoamAction());

        //PrintWorldState();
        ObserveWorldState();
        PrintWorldState();


        current_goal = planner.ChooseAGoal(world_state);
        // Debug.Log("New goal is :"+current_goal.GetType());
        plan = planner.GetAPlan(world_state, current_goal);

        plan_status = "";
        plan_status = "Goal: "+current_goal.GetType()+" / Plan: ";
        for(int i=0; i<plan.Count; i++){
            plan_status += plan.ToArray()[i].GetType() + " --> ";
        }
        plan_status += "DONE";


        // Debug.Log("Plan length = "+plan.Count);
        // while(plan.Count !=0){
        //     AbstractAction to_perfom = plan.Pop();
        //     Debug.Log(to_perfom.GetType());
        // }

        if(plan.Count != 0){
            has_a_plan = true;
            current_action = plan.Pop();
        } else {
            has_a_plan = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        // At every frame we check if the player is nearby, if yes, we dump the plan and get a new one.
        if(current_action.GetType() != typeof(HideInTreeAction)){
            if(PlayerNearbyCheck() && !isInTree){
                Debug.Log("Player is nearby, replannig");
                isPlayerNearby = true;

                has_a_plan = false;
                plan.Clear();

                if(wait){
                    wait=false;
                }
                if(moving){
                    moving = false;
                }

            } else {
                isPlayerNearby = false;
            }
        }

        // CheckIfVisible();


        if(!wait){

            // first, we observe the world state
            ObserveWorldState();

            // if we are moving, we store the nuts and garbages next to us.
            if(moving && current_action.GetType() == typeof(RoamAction)){
                Collider [] objects_in_sight =  Physics.OverlapSphere(gameObject.transform.position, 8f);

                if(objects_in_sight.Length > 0){

                    foreach(Collider coll in objects_in_sight){

                        GameObject item = coll.gameObject;

                        if(item.gameObject.tag == "Nut" && !IsNutInMemory(item)){
                            // Debug.Log("Remembering nuts");
                            if (nuts_in_memory.Count > 4){
                                nuts_in_memory.Dequeue();
                            }
                            nuts_in_memory.Enqueue(item);
                        }
                        if(item.gameObject.tag == "Garbage" && !IsGarbageInMemory(item)){
                            // Debug.Log("Remembering garbage");
                            if (garbage_in_memory.Count > 1){
                                garbage_in_memory.Dequeue();
                            }
                            garbage_in_memory.Enqueue(item);
                        }
                    }
                }

            }

            // if we don't have a plan, we search for one
            if(!has_a_plan){
                PrintWorldState();
                current_goal = planner.ChooseAGoal(world_state);
                // Debug.Log("New goal is :"+current_goal.GetType());
                plan = planner.GetAPlan(world_state, current_goal);

                plan_status = "";
                plan_status = "Goal: "+current_goal.GetType()+" / Plan: ";
                for(int i=0; i<plan.Count; i++){
                    plan_status += plan.ToArray()[i].GetType() + " --> ";
                }
                plan_status += "DONE";
                Debug.Log(plan_status);

                if(plan.Count > 0){
                    has_a_plan = true;
                    current_action = plan.Pop();
                } else {
                    has_a_plan = false;
                }
            }

            // if we are not moving and the action is not finished, we either move or perform the action
            if(!moving && !current_action.FinishedAction ){

                if(current_action.NeedToMove){
                    moving = true;
                    goal_position = GetGoalPosition(current_action);
                    agent.destination = goal_position;

                }else {
                    bool action_status = PerformAction(current_action);

                    // if we could not achieve the action, re plan.
                    if(!action_status){
                        // Debug.Log("Action failed.");
                        has_a_plan = false;
                        plan.Clear();
                    }
                    // otherwise, we set the action to finished
                    else {
                        current_action.SetFinishedAction(true);
                    }
                }
            }

            // if we are moving and we have arrived to destination, we perform the current action
            if(moving && Vector3.Distance(gameObject.transform.position, goal_position) < 1f && !current_action.FinishedAction){
                moving = false;
                bool action_status = PerformAction(current_action);

                // if we could not achieve the action, re plan.
                if(!action_status){
                    // Debug.Log("Action failed.");
                    has_a_plan = false;
                    plan.Clear();
                }
                // otherwise, we set the action to finished
                else {

                    if (current_action.GetType() == typeof(PickNutsAction)){

                        if(GetNumberOfNutsInPocket() > 2){
                            current_action.SetFinishedAction(true);
                        }
                    }
                    else {
                        current_action.SetFinishedAction(true);
                    }
                }
            }

            // if the current action is finished, we either go to the next one, or we flag to get a new goal
            if(current_action.FinishedAction){

                if(plan.Count == 0){
                    has_a_plan = false;
                } else {
                    current_action = plan.Pop();
                }
            }

        }


    }

    private Vector3 GetGoalPosition(AbstractAction action){

        //Debug.Log("Moving to perform "+action.GetType());

        if(action.GetType() == typeof(HideInTreeAction)){
            Debug.Log("Here");
        }

        Vector3 to_return = new Vector3 (0f,0f,0f);

        if(action.GetType() == typeof(PickNutsAction)){
            GameObject next_nut = nuts_in_memory.Peek();
            if(!next_nut.active){
                nuts_in_memory.Dequeue();
                next_nut = nuts_in_memory.Peek();
            }
            to_return = next_nut.gameObject.transform.position;
        }
        else if (action.GetType() == typeof(PickGarbageAction)){
            GameObject next_garbage = garbage_in_memory.Peek();
            Vector3 garbage_pos = next_garbage.gameObject.transform.position;
            to_return = new Vector3(garbage_pos.x +0.3f, garbage_pos.y, garbage_pos.z);
        }
        else if (action.GetType() == typeof(DepositeFoodAction)){
            to_return = new Vector3(home_tree_pos.x + 3f, home_tree_pos.y, home_tree_pos.z);
        }
        else if (action.GetType() == typeof(HideInTreeAction)){

            // we get the closest position
            closest_tree_in_memory = GetCurrentClosestTree();
            // var renderer = closest_tree_in_memory.GetComponent<Renderer>();
            // renderer.material.SetColor("_Color", Color.red);
            Vector3 tree_pos = closest_tree_in_memory.gameObject.transform.position;
            Debug.Log("closest tree coords: "+tree_pos);
            to_return = new Vector3(tree_pos.x+3f, tree_pos.y, tree_pos.z);
        }
        else if (action.GetType() == typeof(RoamAction)){
            // we get a random position in the nav mesh.
            to_return = GameObject.Find("Terrain").GetComponent<Terrain_Manager>().GetRandomPosition();
        }

        return to_return;

    }

    private bool PerformAction(AbstractAction action){
        ObserveWorldState();

        // first we check if we can perform the action
        if(!action.CheckPreConditions(world_state) && action.GetType() != typeof(PickNutsAction)){
            return false;
        }

        if(action.GetType() == typeof(PickNutsAction)){
            // Debug.Log("Performing pick nuts action");

            // we select a nut in the nut memory, add it in our pocket
            GameObject nut = nuts_in_memory.Dequeue();
            food_in_pocket.Add(nut);

            // we remove the nut from its tree.
            nut.SetActive(false);
            nut.transform.parent.gameObject.GetComponent<Tree_Behaviour>().RemoveNut(nut);
            return true;

        }
        else if (action.GetType() == typeof(PickGarbageAction)){
            // Debug.Log("Performing pick garbage action");
            GameObject garbage = garbage_in_memory.Dequeue();

            // we check that there is no squirrel
            if(garbage.gameObject.GetComponent<Garbage_Behaviour>().HasSquirrel){
                return false;
            }

            Vector3 garbage_pos = garbage.gameObject.transform.position;

            if(garbage.gameObject.GetComponent<Garbage_Behaviour>().IsFull == false){

                gameObject.GetComponent<Renderer>().enabled = false;

                garbage.gameObject.GetComponent<Garbage_Behaviour>().SetHasASquirrel(true);
                garbage.gameObject.GetComponent<Garbage_Behaviour>().ShowHiddenSquirrel(true);
                wait = true;
                StartCoroutine(WaitInGarbage(garbage));
                return false;
            } else {
                GameObject content = Instantiate(garbage_content_prefab);
                content.SetActive(false);
                food_in_pocket.Add(content);
                garbage.gameObject.GetComponent<Garbage_Behaviour>().SetIsFull(false);
            }


            return true;

        }
        else if (action.GetType() == typeof(DepositeFoodAction)){
            // Debug.Log("Performing deposit food action");

            while(food_in_pocket.Count > 0){
                food_at_home_tree.Add(food_in_pocket[0]);
                food_in_pocket.RemoveAt(0);
            }

            hasEnergy = false;
            isBackAtHomeTree = true;

            Debug.Log("Squirrel has "+ food_at_home_tree.Count +" items in home tree");
            return true;

        }
        else if (action.GetType() == typeof(HideInTreeAction)){
            // Debug.Log("Performing hide in tree action");

            closest_tree_in_memory = GetCurrentClosestTree();

            if(closest_tree_in_memory.gameObject.GetComponent<Tree_Behaviour>().HasSquirrel){
                return false;
            }else {

                gameObject.GetComponent<Renderer>().enabled = false;

                closest_tree_in_memory.gameObject.GetComponent<Tree_Behaviour>().ShowHiddenSquirrel(true);
                closest_tree_in_memory.gameObject.GetComponent<Tree_Behaviour>().SetHasASquirrel(true);

                hasEnergy = false;
                isInTree = true;
                isPlayerNearby = false;

                return true;
            }

        }
        else if (action.GetType() == typeof(RoamAction)){
            hasEnergy = false;
            isBackAtHomeTree = false;
            return true;
        }
        else if (action.GetType() == typeof(RestAction)){
            // Debug.Log("Performing rest action");
            wait = true;
            StartCoroutine(WaitToRest());
            hasEnergy = true;
            return true;

        }
        else if (action.GetType() == typeof(LeaveTreeAction)){
            // Debug.Log("Performing leave tree action");

            closest_tree_in_memory.gameObject.GetComponent<Tree_Behaviour>().ShowHiddenSquirrel(false);
            closest_tree_in_memory.gameObject.GetComponent<Tree_Behaviour>().SetHasASquirrel(false);

            gameObject.GetComponent<Renderer>().enabled = true;

            isInTree = false;

            return true;

        }
        return false;
    }

    private bool PlayerNearbyCheck(){
        Vector3 player_pos = GameObject.Find("FPSController").transform.position;
        if(Vector3.Distance(gameObject.transform.position, player_pos) < 10f){
            return true;
        }
        return false;
    }

    private bool IsNutInMemory(GameObject new_nut){
        foreach(GameObject known_nut in nuts_in_memory){
            if(Object.ReferenceEquals(new_nut, known_nut)){
                return true;
                break;
            }
        }
        return false;
    }

    private bool IsGarbageInMemory(GameObject new_garbage){
        foreach(GameObject known_garbage in garbage_in_memory){
            if(Object.ReferenceEquals(new_garbage, known_garbage)){
                return true;
                break;
            }
        }
        return false;
    }

    private GameObject GetCurrentClosestTree(){

        GameObject closest_tree = null;
        float smallest_dist = 100000f;
        List<GameObject> all_trees = GameObject.Find("Terrain").GetComponent<Terrain_Manager>().Trees;

        foreach (GameObject t in all_trees){
            if(Vector3.Distance(gameObject.transform.position, t.gameObject.transform.position) < smallest_dist && !t.gameObject.GetComponent<Tree_Behaviour>().HasSquirrel){
                smallest_dist = Vector3.Distance(gameObject.transform.position, t.gameObject.transform.position);
                closest_tree = t;
            }
        }

        return closest_tree;
    }

    private void ObserveWorldState(){
        // < hasEnergy, hasNutsInMemory, hasGarbageInMemory, hasRoomForFood, hasNutInPocket, isPlayerNearby, isInTree>

        // Checking if squirrel has energy
        if(hasEnergy){
            world_state["hasEnergy"] = true;
        }else {
            world_state["hasEnergy"] = false;
        }

        // We remove nuts that are no longer active (have be taken by other squirrels)
        Queue<GameObject> nuts_copy = new Queue<GameObject>();
        while(nuts_in_memory.Count > 0){
            GameObject nut = nuts_in_memory.Dequeue();
            if(nut.active){
                nuts_copy.Enqueue(nut);
            }
        }
        nuts_in_memory = nuts_copy;

        // checking if squirrel has nut in memeory
        if(nuts_in_memory.Count >= 3){
            world_state["hasNutsInMemory"] = true;
        } else {
            world_state["hasNutsInMemory"] = false;
        }

        // checking if squirrel has garbage in memory
        if(garbage_in_memory.Count > 0){
            world_state["hasGarbageInMemory"] = true;
        } else {
            world_state["hasGarbageInMemory"] = false ;
        }

        // checking if squirrel has room for food
        if(food_in_pocket.Count == 0){
            world_state["hasRoomForFood"] = true;
        } else {
            world_state["hasRoomForFood"] = false ;
        }

        // checking if squirrel has a nut in its pocket.
        bool found_a_nut = false;
        foreach(GameObject go in food_in_pocket){
            if(go.gameObject.tag == "Nut"){
                found_a_nut = true;
                break;
            }
        }
        if(found_a_nut){
            world_state["hasNutInPocket"] = true;
        } else {
            world_state["hasNutInPocket"] = false;
        }

        // Checking if player is nearby
        if(isPlayerNearby){
            world_state["isPlayerNearby"] = true;
        }else {
            world_state["isPlayerNearby"] = false;
        }

        // Checking if squirrel is in a tree.
        if(isInTree){
            world_state["isInTree"] = true;
        }else {
            world_state["isInTree"] = false;
        }

        // checking if squirrel is back at his home tree
        if(isBackAtHomeTree){
            world_state["isBackAtHomeTree"] = true;
        }else {
            world_state["isBackAtHomeTree"] = false;
        }

        if(world_state["hasNutsInMemory"] || world_state["hasGarbageInMemory"]){
            world_state["hasSomeFoodInMemory"] = true;
        } else {
            world_state["hasSomeFoodInMemory"] = false;
        }

    }

    private int GetNumberOfNutsInPocket(){
        int counter = 0;
        foreach(GameObject item in food_in_pocket){
            if(item.gameObject.tag == "Nut"){
                counter ++;
            }
        }
        return counter;
    }

    public void SetHomeTreePos(Vector3 designated_tree_pos){
        home_tree_pos = new Vector3(designated_tree_pos.x, designated_tree_pos.y, designated_tree_pos.z);
        // Debug.Log("Home Tree Position : " +home_tree_pos);
    }

    private void PrintWorldState(){
        string s = "< ";
        foreach(KeyValuePair<string, bool> kvp in world_state){
            s = s+kvp.Value+", ";
        }
        s = s+">";

        Debug.Log("Current world state :"+s);
    }

    private IEnumerator WaitToRest(){
        yield return new WaitForSeconds(3);
        wait = false;
    }

    private IEnumerator WaitInGarbage(GameObject garbage){

        yield return new WaitForSeconds(3);

        gameObject.GetComponent<Renderer>().enabled = true;
        garbage.gameObject.GetComponent<Garbage_Behaviour>().SetHasASquirrel(false);
        garbage.gameObject.GetComponent<Garbage_Behaviour>().ShowHiddenSquirrel(false);
        wait = false;
    }

    public string GetPlanStatus(){
        return plan_status;
    }

    // private void CheckIfVisible(){
    //     if(!isInTree || !wait){
    //         gameObject.GetComponent<Renderer>().enabled = true;
    //     }
    // }

}
