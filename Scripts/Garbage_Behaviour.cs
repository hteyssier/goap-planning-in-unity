using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garbage_Behaviour : MonoBehaviour
{
    public GameObject squirrel_to_show_prefab;

    private List<GameObject> nuts_in_garbage;
    private GameObject squirrel_on_top;

    private bool has_a_squirrel;
    private bool wait;
    private bool is_full;

    // Start is called before the first frame update
    void Start()
    {
        
        squirrel_on_top = Instantiate(squirrel_to_show_prefab, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 2f, gameObject.transform.position.z), Quaternion.identity);
        squirrel_on_top.SetActive(false);
        // We initialize some variables
        nuts_in_garbage = new List<GameObject>();
        has_a_squirrel = false;
        wait = false;

        // State of the garbage bin is randomly initialized
        int random_init = UnityEngine.Random.Range(0,2);
        if(random_init==0){
            is_full = false;
        }else {
            is_full = true;
        }
        ChangeColor();
        
        
    }

    // Update is called once per frame
    void Update()
    {
        // Every 10 seconds, if there are no squirrels, we change the state
        if(!wait){
            if(!has_a_squirrel){
                if(is_full){
                    is_full = false;
                } else {
                    is_full = true;
                }
                ChangeColor();
            }
            wait = true;
            StartCoroutine(WaitFor10Seconds());
        }
        
    }

    public void ShowHiddenSquirrel(bool b){
        if (b){
            squirrel_on_top.SetActive(true);
        } else {
            squirrel_on_top.SetActive(false);
        }

    }

    private IEnumerator WaitFor10Seconds(){
        yield return new WaitForSeconds(10);
        wait=false;
    }

    private void ChangeColor(){
        var renderer = gameObject.GetComponent<Renderer>();
        if(is_full){
            renderer.material.SetColor("_Color", Color.red);
        }else {
            renderer.material.SetColor("_Color", Color.white);
        }
    }

    public void SetHasASquirrel(bool b){
        has_a_squirrel = b;

    }

    public void SetIsFull(bool b){
        is_full = b;
        ChangeColor();
    }

    public bool HasSquirrel{
        get {
            return has_a_squirrel;
        }
    }

    public bool IsFull {
        get {
            return is_full;
        }
    }
}
