using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using System;

public class Terrain_Manager : MonoBehaviour
{

    public GameObject first_tree_prefab;
    public GameObject second_tree_prefab;    
    public GameObject bin_prefab;
    public GameObject squirrel_prefab;

    public GameObject squirrel_1_test_obj;
    public GameObject squirrel_2_test_obj;
    public GameObject squirrel_3_test_obj;
    public GameObject squirrel_4_test_obj;
    public GameObject squirrel_5_test_obj;

    private Text squir1_text;
    private Text squir2_text;
    private Text squir3_text;
    private Text squir4_text;
    private Text squir5_text;

    private string str1;
    private string str2;
    private string str3;
    private string str4;
    private string str5;


    private List<Vector3> taken_position;
    private List<GameObject> trees;
    private List<GameObject> squirrels; 

   
    // Start is called before the first frame update
    void Start()
    {
        taken_position = new List<Vector3>();
        trees = new List<GameObject>();
        squirrels = new List<GameObject>();

        GenerateTreesAndBins();
        GenerateSquirrels();
        Debug.Log("Terrain - Done generating layout.");

        squir1_text = squirrel_1_test_obj.gameObject.GetComponent<Text>();
        squir2_text = squirrel_2_test_obj.gameObject.GetComponent<Text>();
        squir3_text = squirrel_3_test_obj.gameObject.GetComponent<Text>();
        squir4_text = squirrel_4_test_obj.gameObject.GetComponent<Text>();
        squir5_text = squirrel_5_test_obj.gameObject.GetComponent<Text>();

        str1 = "";
        str2 = "";
        str3 = "";
        str4 = "";
        str5 = "";
        
    }

    // Update is called once per frame
    void Update()
    {
        squir1_text.text = str1;
        squir2_text.text = str2;
        squir3_text.text = str3;
        squir4_text.text = str4;
        squir5_text.text = str5;

        str1 = squirrels[0].gameObject.GetComponent<Squirrel_Behaviour>().GetPlanStatus();
        str2 = squirrels[1].gameObject.GetComponent<Squirrel_Behaviour>().GetPlanStatus();
        str3 = squirrels[2].gameObject.GetComponent<Squirrel_Behaviour>().GetPlanStatus();
        str4 = squirrels[3].gameObject.GetComponent<Squirrel_Behaviour>().GetPlanStatus();
        str5 = squirrels[4].gameObject.GetComponent<Squirrel_Behaviour>().GetPlanStatus();
        
    }

    public Vector3 GetRandomPosition(){

        float x_coord = UnityEngine.Random.Range(5.0f, 95.0f);
        float z_coord = UnityEngine.Random.Range(5.0f, 45.0f);
        Vector3 a_random_position = new Vector3(x_coord,0f,z_coord);
        
        NavMeshHit hit;

        while(!NavMesh.SamplePosition(a_random_position, out hit, 1f, NavMesh.AllAreas)){
            x_coord = UnityEngine.Random.Range(5.0f, 95.0f);
            z_coord = UnityEngine.Random.Range(5.0f, 45.0f);
            a_random_position = new Vector3(x_coord,0f,z_coord);
        }
        
        return a_random_position;
    }

    private void GenerateSquirrels(){

        List<int> taken_tree_indices = new List<int>();
        
        // We instantiate 5 squirrels 
        for (int i=0; i<5; i++){
            
            // First we select a valid home tree, i.e. each squirrel needs a different home tree.
            int tree_index = UnityEngine.Random.Range(0,trees.Count);
            bool found_valid_index = false;

            while(!found_valid_index){
                
                if(taken_tree_indices.Count==0){
                    found_valid_index = true;
                }

                foreach(int taken_index in taken_tree_indices){
                    if(tree_index == taken_index){
                        tree_index = UnityEngine.Random.Range(0,trees.Count);
                        found_valid_index = false;
                        break;
                    }
                    found_valid_index = true;
                }
            }
            taken_tree_indices.Add(tree_index);

            // We now create a squirrel.
            GameObject a_random_home_tree = trees[tree_index];
            // var renderer = a_random_home_tree.GetComponent<Renderer>();
            // renderer.material.SetColor("_Color", Color.red);
            int r = 5;
            float theta = UnityEngine.Random.Range(0.0f, 1.0f) * 2f * (float)Math.PI;
            float x_coord = a_random_home_tree.gameObject.transform.position.x + r * (float)Math.Cos(theta);
            float z_coord = a_random_home_tree.gameObject.transform.position.z + r * (float)Math.Sin(theta);
            GameObject a_new_squirrel = Instantiate(squirrel_prefab, new Vector3(x_coord, 0.02f,z_coord), Quaternion.identity);

            //a_new_squirrel.GetComponent<Squirrel_Behaviour>().SetHomeTreePos(a_random_home_tree.gameObject.transform.position);
            squirrels.Add(a_new_squirrel);
        }
    }

    private void GenerateTreesAndBins(){

        // Generating Trees
        for(int i=0; i<10; i++){
            
            Vector3 tree_position = new Vector3(0f, 0f, 0f);
            // looking for a valid position
            while(true){
                float x_coord = UnityEngine.Random.Range(5.0f, 95.0f);
                float z_coord = UnityEngine.Random.Range(5.0f, 45.0f);
                bool overlaped = false; 
                foreach (Vector3 a_taken_pos in taken_position){
                    if(Math.Abs(x_coord - a_taken_pos.x) < 10.0f && Math.Abs(z_coord - a_taken_pos.z) < 10.0f){
                        overlaped = true;
                        break;
                    }
                }
                if(!overlaped){
                    tree_position = new Vector3(x_coord, 0f, z_coord);
                    taken_position.Add(tree_position);
                    break;
                }
            }

           
            trees.Add(Instantiate(first_tree_prefab, tree_position, Quaternion.identity));
            
        }

        // Generating garbage bins 
        for (int i=0; i<5; i++){
            Vector3 bin_position = new Vector3(0f, 0f, 0f);

            // looking for a valid position
            while(true){
                float x_coord = UnityEngine.Random.Range(5.0f, 95.0f);
                float z_coord = UnityEngine.Random.Range(5.0f, 45.0f);
                bool overlaped = false; 
                foreach (Vector3 a_taken_pos in taken_position){
                    if(Math.Abs(x_coord - a_taken_pos.x) < 8.0f && Math.Abs(z_coord - a_taken_pos.z) < 8.0f){
                        overlaped = true;
                        break;
                    }
                }
                if(!overlaped){
                    bin_position = new Vector3(x_coord, 0f, z_coord);
                    taken_position.Add(bin_position);
                    break;
                }
            }
            
            Instantiate(bin_prefab, bin_position, Quaternion.identity);

        }

    }

    public List<GameObject> Trees{
        get {
			return trees;
		}
    }

}
