# GOAP Planning in Unity 

Scripts for an implementation of an AI agent for NPCs in a game, implemented using Unity Game Engine. 

## Concept

The goal of the game is to implement an AI agent for NPCs. The game terrain has trees as well as garbage cans. Trees drop nuts next to them. NPCs correspond to squirrels and they have several goals to achieve. One is getting food by either looking for nuts or by searching inside garbage cans. Another goal is to flee from the player if he aproaches too close. Finally, NPCs also have two idle goals, which are resting and randomly exploring the terrain. 

Each squirrel has a short-term memory. He remembers the 5 closest nuts and the 2 closest garbage cans. From the observed state of the world, the NPC chooses a goal and try to come up with a plan that consists of a succession of actions. The implementation makes use of GOAP planning to achieve that. 

## Actions and Goals

Here are the components of the world vector:

< hasEnergy, hasSomeFoodInMemory, hasNutsInMemory, hasGarbageInMemory, hasRoomForFood, hasNutInPocket, isPlayerNearby, isInTree, isBackAtHomeTree>

- hasEnergy tells us if the agent has energy
- hasSomeFoodInMemory indicates us if the agent has at least nuts or garbages in its memory
- hasNutsInMemory indicates us if the agent has at least 3 nuts in its memory
- hasGarbageInMemory indicates us if the agent has at least 1 garbage in its memory
- hasRoomForFood indicates us if the agent has at most 2 items on him.
- hasNutsInPocket indicates us if the agent has at least one nut on him.
- isPlayerNearby indicates us if the player is nearby
- isInTree indicates us if the agent is in a tree (namely hiding)
- isBackAtHomeTree indicates us if the agent is back at its home tree. 

The GOAP planner is given a world state as input and chooses a goal by checking each goal pre-conditions. 
We have two idle goals: « roam » and « rest ».
We also have two normal goals: « flee from player » and « make reserve ».

Choose a goal from:
- **make reserve**       
    pre: < T, T, -, -, T, -, F, -, - >          
    post: < F, -, -, -, T, -, -, -, T >
- **flee from player**    
    pre: < -, -, —, -, -, -, T, F, - >          
    post: < F, -, -, -, -, -, F, T, - >
- **roam**             
    pre: < T, -, -, -, -, -, F, -, - >          
    post: < F, -, -, -, -, -, -, -, F >           
- **rest**                
    pre: < F, —, -, -, -, -, F, -, - >          
    post: < T, -, -, -, -, —, -, -, - >

Once we selected a goal, we perform a search on all actions to go from the current world state to the post condition world state of the desired goal. 

Available Actions:
- **pickNutsAction**     (cost 3)        
    pre: < T, -, T, -, T, -, F, F, - >          
    effect: < -, —, -, -, F, T, -, - , - >         
- **pickGarbageAction**  (cost 2)        
    pre: < T, —, -, T, T, F, F, F, - >          
    effect: < -, -, -, -, F, -, -, - , - >        
- **depositeFoodAction** (cost 2)        
    pre: < T, -, -, -, F, -, F, F, - >          
    effect: < F, -, -, -, T, -, -, - , T >
- **hideInTreeAction**   (cost 1)        
    pre: < -, -, -, -, -, -, T, F, - >         
    effect: < F, -, -, -, -, -, F, T , - >	
- **leaveTreeAction**    (cost 1)        
    pre: < -, -, -, -, -, -, F, T, - >          
    effect: < -, -, -, -, -, -, -, F , - >
- **restAction**		 (cost 0)        
    pre: < F, -, -, -, -, -, F, -, - >		    
    effect: < T, -, -, -, -, -, -, - , - >
- **roamAction**		 (cost 2)		
    pre: < T, -, -, -, -, -, F, F, - >		    
    effect: < F, -, -, -, -, -, -, - , F >			


**Behaviours examples:**

The idle behaviour we would consists either in a restAction or in a roamAction.
The flee from player behaviour, we would consists in a hideInTreeAction.
The pick nuts behaviour could consists in (potentially a leaveTreeAction) a pickNutsAction followed by a depositFoodAction
The gather food from garbage cans behaviour could consists in (potentially a leaveTreeAction) a pickGarbageAction followed by a depositFoodAction

## Usability

Only scripts are shared on this repo because I was using dowloaded assets that I do not have the right to share. Feel free to copy the different scripts and improve them. 

## Author

Hector Teyssier - 260840287

Comp 521 - Fall 2021 - McGill University








